package ordenamientoGUI;

public class Ordenamientos {
	
	public static void burbuja(int[] digitos) {
		for (int i = 0; i < (digitos.length - 1); i++)
			for (int j = 0; j < (digitos.length - i - 1); j++)
				if (digitos[j + 1] < digitos[j]) {
					int aux = digitos[j + 1];
					digitos[j + 1] = digitos[j];
					digitos[j] = aux;
				}
	}

}
