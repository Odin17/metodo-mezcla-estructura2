package Ejercicio1;
import static java.lang.System.gc;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Scanner;


public class Main {
	static Scanner leer=new Scanner(System.in);
	static int[] lista;
	
	public static void main(String[] args){
                               
		menuStart();
	}		
	private static void menuStart(){
		System.out.print("Algoritmo de Ordenamiento--1");
		System.out.println("\n1.-Ejercicio");
		int opc=leer.nextInt();
		if(opc==1){
			System.out.println("Ingresa el Tamaño Del Arreglo : ");
			int cant=leer.nextInt();
			int[] origin=genArr(cant);
			System.out.println("Arreglo Creado!..\n"+Arrays.toString(origin));
			menuSorts(origin);
		}
                
	}
	
	private static void menuSorts(int[] arreglo){

		System.out.println("1.-Comenzar");
		System.out.println("2.-Atras");
                System.out.println("3.-Salir");
		
		int opc=leer.nextInt();
		switch(opc){
			
		case 1:
                       long startTime = System.nanoTime();
		       startTime = System.nanoTime();
		       System.out.println("1.-Ordenamiento por Radix-> "+Arrays.toString(Sorts.radix(arreglo)));
                       long time = System.nanoTime() - startTime;
		       time = System.nanoTime() - startTime;
		       System.out.println("Tiempo: "+time+" Nanoseg...\n"); 
		       menuSorts(arreglo);
		       break;
                case 2:
                    
			   menuStart();
			   break;
		default:
			break;
		}
	}
	public static int[] genArr(int cant){//Crea un arreglo Random
		int arr[]=new int[cant];
		for(int i=0;i<cant;i++){
			arr[i]=(int)(Math.random()*500+1);
		}		
		return arr;
        }
    }

 