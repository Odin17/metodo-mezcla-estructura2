package mezcla;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import mezcla.Ordenamientos;

public class Interfaz extends JFrame{

	private JTextField[] digitosEntrada; //Array con los campos de texto para los digitos
	private JLabel[] digitosOrdenados; //Array con los label donde se mostrarán los digitos ordenados
	private JButton btRestablecer; //Botón para restablecer formulario
	//Botones para cada tipo de ordenamiento
	private JButton btMezcla;

	//Constructor
	public Interfaz() {
		super("Ordenamiento Digitos");
		inicializarComponentes();

		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		c.add(new PanelTitulo(), BorderLayout.NORTH);
		JPanel centro = new JPanel();
		centro.setLayout(new BoxLayout(centro, BoxLayout.Y_AXIS));
		centro.add(new PanelEntradaDigitos());
		centro.add(new PanelOrdenamientoDigitos());
		c.add(centro, BorderLayout.CENTER);
		c.add(new PanelBotonera(), BorderLayout.SOUTH);

		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	//Métodos
	
	/*
	 * Inicializa todos los componentes del formulario.
	 */
	private void inicializarComponentes() {
		digitosEntrada = new JTextField[10];
		digitosOrdenados = new JLabel[10];
		for (int i = 0; i < 10; i++) {
			digitosEntrada[i] = new JTextField(1);
			digitosEntrada[i].setFont(new Font("Verdana", Font.PLAIN, 20));
			digitosEntrada[i].addKeyListener(new EntradaDigito());
			digitosOrdenados[i] = new JLabel(" ");
			digitosOrdenados[i].setFont(new Font("Verdana", Font.BOLD, 20));
		}
		btRestablecer = new JButton("<html><p align=\"center\">Restablecer<br>Formulario</p></html>");
		btRestablecer.addActionListener(new AccionRestablecer());
		btMezcla = new JButton("<html><p align=\"center\">Ordenamiento<br>Mezcla</p></html>");
		btMezcla.addActionListener(new AccionMezcla());
		activarBotones(false);
	}

	/*
	 * Activa/Desactiva los botones para escoger ordenamiento.
	 * Se activan cuando todos los campos de texto tienen un digito.
	 * Se desactivan cuando algún campo de texto se queda sin digito
	 * porque lo ha borrado el usuario o también cuando se restablece
	 * el formulario.
	 */
	private void activarBotones(boolean activar) {
		btMezcla.setEnabled(activar);
	}
	
	/*
	 * Lee todos los digitos introducidos por el usuario
	 * y los retorna dentro de un array.
	 */
	private int[] recogeDigitos() {
		int[] digitos = new int[10];
		for (int i = 0; i < 10; i++)
			digitos[i] = Integer.parseInt(digitosEntrada[i].getText());
		
		return digitos;
	}
	
	/*
	 * Recibe un array con los digitos YA ordenados y
	 * los muestra en el panel correspondiente 
	 */
	private void muestraDigitosOrdenados(int[] digitos) {
		for (int i = 0; i < 10; i++)
			digitosOrdenados[i].setText(String.valueOf(digitos[i]));
	}

	public static void main(String[] args) {
		new Interfaz();
	}

	//Clases Paneles
	
	/*
	 * Panel superior con título de enunciado
	 */
	class PanelTitulo extends JPanel {
		public PanelTitulo() {
			add(new JLabel("Introduzca dígitos y escoja método de ordenamiento "));
		}
	}

	/*
	 * Panel donde se teclean los digitos 
	 */
	class PanelEntradaDigitos extends JPanel {
		public PanelEntradaDigitos() {
			setBorder(new CompoundBorder(new TitledBorder("Entrada de Digitos"), new EmptyBorder(20, 10, 20, 10)));
			setLayout(new FlowLayout(FlowLayout.CENTER, 30, 10));
			for (int i = 0; i < 10; i++) {
				add(digitosEntrada[i]);
			}
		}
	}

	/*
	 * Panel donde se mostrarán los digitos ordenados
	 */
	class PanelOrdenamientoDigitos extends JPanel {
		public PanelOrdenamientoDigitos() {
			setBorder(new CompoundBorder(new TitledBorder("Ordenamiento de Digitos"), new EmptyBorder(20, 10, 20, 10)));
			setLayout(new FlowLayout(FlowLayout.CENTER, 40, 10));
			for (int i = 0; i < 10; i++) {
				add(digitosOrdenados[i]);
			}
		}
	}

	/*
	 * Panel con los botones
	 */
	class PanelBotonera extends JPanel {
		public PanelBotonera() {
			add(btRestablecer);
			add(btMezcla);
		}
	}

	//Clases Listener

	/*
	 * Listener para el botón de restablecer formulario
	 */
	class AccionRestablecer implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			for (int i = 0; i < 10; i++) {
				digitosEntrada[i].setText(null);
				digitosOrdenados[i].setText(" ");
				activarBotones(false);
			}
		}
	}
	
	/*
	 * Listener para el botón de ordenamiento mediante
	 * método de la mezcla.
	 */
	class AccionMezcla implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int[] digitos = recogeDigitos();
			Ordenamientos.ordenar(digitos);
			muestraDigitosOrdenados(digitos);
		}
	}

	/*
	 * Listener para controlar la entrada de digitos.
	 * Solo admitirá digitos de 0 a 9 y solo un único
	 * digito para cada campo de texto.
	 * Además comprueba si los botones para escoger ordenamiento
	 * se han de activar o desactivar, según si los campos
	 * tienen todos ya un digito o no.
	 */
	class EntradaDigito implements KeyListener {
		@Override
		public void keyTyped(KeyEvent e) {
			//Comprobamos digito tecleado
			char digito = e.getKeyChar();
			if (digito < '0' || digito > '9')
				e.consume(); //Rechazamos lo tecleado porque no es un digito

			//Comprobamos si el campo tiene ya un digito anterior
			JTextField campo = (JTextField) e.getComponent();
			if (!campo.getText().isEmpty())
				e.consume(); //Rechazamos lo tecleado porque ya contiene un digito
		}

		@Override
		public void keyPressed(KeyEvent e) {
			//Nada que hacer en este método
		}

		@Override
		public void keyReleased(KeyEvent e) {
			//Al soltar tecla, comprobamos si todos los campos tienen ya un digito.
			//Si todos lo tienen, hay que activar botones de accion
			//Si alguno no tiene, nos aseguramos de que se desactiven
			boolean activar = true;
			for (int i = 0; i < 10; i++) {
				if (digitosEntrada[i].getText().isEmpty()) //Al menos uno, esta vacio...
					activar = false; //...así que hay que desactivar los botones de ordenamiento
			}
			activarBotones(activar);
		}
	}

}
